package TP2048;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.Font;
//import java.util.Random;

import java.awt.EventQueue;

public class Interfaz {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTable table = new JTable();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interfaz window = new Interfaz();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Interfaz() {

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

		}

		catch (Exception e) {
			System.out.println("Error setting native LAF:" + e);
		}
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		Tabla tabla = new Tabla();
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lbl2048 = new JLabel("2048");
		lbl2048.setBounds(34, 11, 88, 29);
		lbl2048.setFont(new Font("Calibri", Font.PLAIN, 25));
		frame.getContentPane().add(lbl2048);

		JLabel lblPuntuacion = new JLabel("Puntuación");
		lblPuntuacion.setBounds(224, 11, 96, 14);
		lblPuntuacion.setFont(new Font("Calibri", Font.PLAIN, 15));
		frame.getContentPane().add(lblPuntuacion);

		JLabel lblRecord = new JLabel("Récord");
		lblRecord.setBounds(351, 11, 96, 14);
		lblRecord.setFont(new Font("Calibri", Font.PLAIN, 15));
		frame.getContentPane().add(lblRecord);

		textField = new JTextField();
		textField.setEditable(false);
		textField.setBounds(213, 25, 96, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setEnabled(false);
		textField_1.setEditable(false);
		textField_1.setBounds(330, 25, 96, 20);
		textField_1.setColumns(10);
		frame.getContentPane().add(textField_1);
		table.setEnabled(false);
		table.setColumnSelectionAllowed(true);
		table.setSurrendersFocusOnKeystroke(true);
		table.setBounds(109, 91, 166, 140);
		frame.getContentPane().add(table);

		table.setModel(new DefaultTableModel(
				new Object[][] { { null, null, null, null }, { null, null, null, null }, { null, null, null, null },
						{ null, null, null, null }, },
				new String[] { "New column", "New column", "New column", "New column" }));

		table.setRowHeight(35);

		TableModel model = table.getModel();
		model.setValueAt(tabla.getBloq1Inicial().numero, tabla.getBloq1Inicial().getPosF(),
				tabla.getBloq1Inicial().getPosC());
		model.setValueAt(tabla.getBloq2Inicial().numero, tabla.getBloq2Inicial().getPosF(),
				tabla.getBloq2Inicial().getPosC());

	}

}
