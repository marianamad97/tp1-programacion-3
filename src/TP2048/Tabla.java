package TP2048;

import java.util.Random;

public class Tabla {

	private Bloque[][] tabla = new Bloque[4][4];
	private Bloque b1Inicial, b2Inicial;
	Random random = new Random();

	Tabla() {
		this.b1Inicial = new Bloque();
		this.b2Inicial = new Bloque();
		this.tablaInicial();

	}

	public void tablaInicial() {

		for (int f = 0; f < this.tabla.length; f++) {
			for (int c = 0; c < this.tabla[0].length; c++) {
				if (f == this.b1Inicial.getPosF() && c == this.b1Inicial.getPosC()) {
					this.tabla[f][c] = this.b1Inicial;
					this.tabla[f][c].numero = this.b1Inicial.getNumero();

				}
				if (f == this.b2Inicial.getPosF() && c == this.b2Inicial.getPosC()) {
					this.tabla[f][c] = this.b2Inicial;
					this.tabla[f][c].numero = this.b2Inicial.getNumero();

				}
			}
		}

	}

	// -------------------------------------------------------------------------

	public Bloque getBloq1Inicial() {
		return this.b1Inicial;
	}

	public Bloque getBloq2Inicial() {
		return this.b2Inicial;
	}

}
