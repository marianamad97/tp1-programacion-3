package TP2048;

import java.util.Random;

public class Bloque {

	public int[] numerosInicio = { 2, 4 };
	// public int [] numerosGeneral= {2, 4, 8, 16, 32, 64, 128, 256, 512, 1024,
	// 2048};
	public int numero;
	public int posF, posC; // posición en que se ubica en la matriz/tabla

	Random random = new Random();

	Bloque() {
		this.numero = generarNumeroRandom();
		this.posF = random.nextInt(4);
		this.posC = random.nextInt(4);
	}

	public int getPosF() {
		return this.posF;

	}

	public int getPosC() {
		return this.posC;

	}

	private int generarNumeroRandom() {
		int indiceAleatorio = random.nextInt(2);
		int aleatorioEntre2y4 = this.numerosInicio[indiceAleatorio];
		return aleatorioEntre2y4;
	}

	public int getNumero() {
		return this.numero;
	}

	public boolean mismoNro(int n1, int n2) { // Verifica si dos bloques tienen el mismo número
		if (n1 == n2)
			return true;
		else
			return false;
	}

	public int sumarBloques(int nroBloque) {// Suma dos bloques que tienen el mismo número
		return 2 * nroBloque;
	}

	public boolean mismaPos(Bloque b1, Bloque b2) {
		// Método utilizado para que no se generen bloques en una posicion ocupada de la
		// tabla
		if (b1.getPosF() == b2.getPosF() && b1.getPosC() == b2.getPosC())
			return true;
		else
			return false;
	}
}
